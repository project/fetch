<?php

/**
 * @file
 * Fetch IMAP Integration administrative pages.
 */

/**
 * Administrative page listing Fetch IMAP accounts.
 */
function fetch_admin_page() {
  $page = array();

  $accounts = db_query('SELECT * FROM {fetch_accounts} ORDER BY `name`')->fetchAll();

  $rows = array();
  foreach ($accounts as $account) {
    $row = array();
    $row[] = l($account->name, 'admin/config/system/fetch/' . $account->machine_name);
    $row[] = check_plain("{$account->server}:{$account->port}");
    $actions = array();
    $actions[] = l('Edit', 'admin/config/system/fetch/' . $account->machine_name . '/edit');
    $actions[] = l('Delete', 'admin/config/system/fetch/' . $account->machine_name . '/delete');
    $row[] = implode(' ', $actions);
    $rows[] = $row;
  }

  $page['accounts'] = array(
    '#theme' => 'table',
    '#caption' => t('IMAP Accounts'),
    '#header' => array(t('Name'), t('Server'), ''),
    '#empty' => t('No accounts.'),
    '#rows' => $rows,
  );

  return $page;
}

/**
 * Fetch IMAP account details page.
 */
function fetch_account_view_page($account) {
  drupal_set_title($account->name);

  $rows = array();
  $rows[] = array(t('Machine Name'), check_plain($account->machine_name));
  $rows[] = array(t('Login'), check_plain($account->login));
  $rows[] = array(t('Password'), '***');
  $rows[] = array(t('Server'), check_plain($account->server));
  $rows[] = array(t('Port'), check_plain($account->port));
  $rows[] = array(t('Flags'), check_plain(implode(', ', array_filter($account->flags))));
  $rows[] = array(t('Service'), check_plain($account->service));

  $output['info'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
  );

  $output['test'] = drupal_get_form('fetch_imap_test_form', $account);

  return $output;
}
